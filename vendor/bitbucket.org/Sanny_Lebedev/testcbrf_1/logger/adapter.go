package logger

import (
	"github.com/jackc/pgx"
	"github.com/rs/zerolog"
)

// Logger is a logger interface for pgx and goa
type Logger struct {
	zerolog.Logger
}
type LogLevel int

// Log is a function to satisfy pgx logger interface
func (l Logger) Log(loglevel pgx.LogLevel, msg string, data map[string]interface{}) {
	var ev *zerolog.Event
	switch loglevel {
	case pgx.LogLevelDebug:
		ev = l.Debug()
	case pgx.LogLevelError:
		ev = l.Error()
	case pgx.LogLevelInfo:
		ev = l.Info()
	case pgx.LogLevelWarn:
		ev = l.Warn()
	default:
		ev = l.Debug()
	}
	for k, v := range data {
		if k == "time" {
			continue
		}
		ev = ev.Interface(k, v)
	}
	ev.Msg(msg)
}
