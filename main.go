package main

import (
	"context"
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"bitbucket.org/Sanny_Lebedev/testcbrf_1/logger"

	"google.golang.org/api/sheets/v4"

	"github.com/crgimenes/goconfig"
	_ "github.com/crgimenes/goconfig/json"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func (c *Config) getCours() Answer {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	var result Answer
	url := c.Bank.URL

	NewPayload := strings.Replace(c.Bank.Payload, "[CURRENTDATE]", time.Now().Format("2006-01-02"), -1)
	payload := strings.NewReader(NewPayload)
	req, err := http.NewRequest("POST", url, payload)
	if err != nil {
		c.Log.Error().Err(err).Msg("Ошибка запроса к банку")
		cancel()
	}

	req.Header.Add("Content-Type", c.Bank.ContentType)
	req.Header.Add("SOAPAction", c.Bank.SOAPAction)
	req = req.WithContext(ctx)
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		c.Log.Error().Err(err).Msg("Ошибка запроса к банку")
		cancel()
	}

	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)

	res1 := &MyRespEnvelope{}
	_ = xml.Unmarshal([]byte(body), res1)

	for _, element := range res1.Body.GetResponse.GetResult.Diffgram.GetData.GetCurs {
		if element.Vcode == "840" {
			c.Log.Info().Str("Курс USD", element.Vcurs).Time("Time", time.Now()).Msg("Текущий курс")
			result = Answer{time.Now(), element.Vcurs}
			cancel()
			return result
		}
	}
	cancel()
	return Answer{}
}

func initConfig() (*configMain, error) {
	myconfig := configMain{}
	goconfig.File = "config.json"
	err := goconfig.Parse(&myconfig)
	if err != nil {

		log.Error().Err(err)
		return nil, err
	}
	return &myconfig, nil
}

func initial() *Config {

	IntervalOut := 0
	env := Env{}
	env.myConf, _ = initConfig()
	log := logger.Logger{
		Logger: zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger(),
	}
	inte := os.Getenv("Interval")
	if len(inte) == 0 {
		log.Warn().Msg("Переменная окружения для интервала не найдена")

	} else {
		log.Info().Str("Интервал ", inte).Msg("Переменная окружения")
		i, err := strconv.Atoi(inte)

		if err != nil {
			log.Fatal().Str("Значение ", inte).Msg("Неверный интервал")
		}
		IntervalOut = i
	}

	log.Info().Str("Инициализация ", "OK").Time("Time", time.Now()).Msg("System init")

	return &Config{
		Log:      log,
		Bank:     env.myConf.Bank,
		Sheet:    env.myConf.Sheet,
		interval: IntervalOut,
	}
}

func main() {

	conf := initial()

	srv, errS := conf.InitSheet()
	if errS == nil {
		conf.Log.Info().Msg("Найден токен работы с таблицами Google")
	}

	var Wg sync.WaitGroup
	jobs := make(chan ForWorker, 10)

	for i := 0; i < 10; i++ {
		Wg.Add(1)
		go wait(i, jobs, &Wg, conf, srv)
	}

	TickerInterval := time.Second * 10

	if conf.interval != 0 {
		TickerInterval = time.Hour * time.Duration(conf.interval)
	}

	ticker := time.NewTicker(TickerInterval)

	for {

		select {
		case <-ticker.C:
			jobs <- ForWorker{Action: GetActualData}
		}

	}

}

func wait(id int, jobs chan ForWorker, Wg *sync.WaitGroup, c *Config, srv *sheets.Service) {
	defer Wg.Done()
	for job := range jobs {
		switch job.Action {

		//Getting only one job - GetActualData
		case GetActualData:
			c.Log.Info().Int("Worker", id).Msg("Запущена обработка")
			c.SheetsAppend(srv, c.getCours())
		}
	}
}
