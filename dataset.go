package main

import (
	"encoding/xml"
	"time"

	"bitbucket.org/Sanny_Lebedev/testcbrf_1/logger"
)

type (
	systemAPP struct {
		Name string `json:"name" cfg:"name"`
		Port string `json:"port" cfg:"port"`
	}

	//Bank - settings for request from config
	Bank struct {
		ContentType string `json:"Content-Type" cfg:"Content-Type"`
		SOAPAction  string `json:"SOAPAction" cfg:"SOAPAction"`
		URL         string `json:"URL" cfg:"URL"`
		Payload     string `json:"Payload" cfg:"Payload"`
	}

	//Sheet - config for google sheets
	Sheet struct {
		SpreadsheetId string `json:"SpreadsheetId" cfg:"SpreadsheetId"`
	}

	configMain struct {
		DebugMode bool `json:"debug" cfg:"debug" cfgDefault:"false"`
		Domain    string
		APP       systemAPP `json:"app" cfg:"app"`
		Bank      Bank      `json:"bank" cfg:"bank"`
		Sheet     Sheet     `json:"Sheet" cfg:"Sheet"`
	}

	//Config - main current configuration
	Config struct {
		Log      logger.Logger
		Bank     Bank `json:"bank" cfg:"bank"`
		Sheet    Sheet
		interval int
	}

	//Env - structure for config.json
	Env struct {
		myConf *configMain
	}

	//ForWorker Structure for chan
	ForWorker struct {
		Action string `json:"action"`
	}

	//MyRespEnvelope - for parsing xml
	MyRespEnvelope struct {
		XMLName xml.Name
		Body    Body
	}

	//Body - for parsing xml
	Body struct {
		XMLName     xml.Name
		GetResponse completeResponse `xml:"GetCursOnDateResponse"`
	}

	completeResponse struct {
		XMLName   xml.Name
		GetResult completeResult `xml:"GetCursOnDateResult"`
	}

	completeResult struct {
		Schema   interface{} `xml:"schema"`
		Diffgram diffgram    `xml:"diffgram"`
	}

	diffgram struct {
		GetData completeData `xml:"ValuteData"`
	}

	completeData struct {
		GetCurs []completeCurs `xml:"ValuteCursOnDate"`
	}
	completeCurs struct {
		Vname   string `xml:"Vname"`
		Vnom    string `xml:"Vnom"`
		Vcurs   string `xml:"Vcurs"`
		Vcode   string `xml:"Vcode"`
		VchCode string `xml:"VchCode"`
	}

	//Answer - result
	Answer struct {
		Time   time.Time `json:"time"`
		Course string    `json:"Course"`
	}
)

const (
	// GetActualData ...
	GetActualData = "getData"
	// CtxTimeOut - time limit for request in seconds
	CtxTimeOut = 10
)

// //Worker ...
// type Worker struct {
// 	id int
// 	db *pgx.ConnPool
// }

// //Chan ...
// type Chan struct {
// 	uid       int64
// 	confirmed bool
// }

// type message struct {
// 	responseChan chan<- string
// 	parameter    string
// 	ctx          context.Context
// }
